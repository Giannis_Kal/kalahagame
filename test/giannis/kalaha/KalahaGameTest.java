package giannis.kalaha;

import static org.junit.Assert.*;

import org.junit.Test;
//This class contains some example test cases.
public class KalahaGameTest {
	KalahaGame testBoard = new KalahaGame ();
	@Test
	public void testIsHuman() {
		assertEquals(testBoard.isHuman(), false);
	}
	
	@Test
	public void testgetScore() {
		assertEquals(testBoard.getScore(), 0);
	}
	
	@Test
	public void testIndexOfLastStone() {
		assertEquals(testBoard.getScore(), 0);
		assertEquals(testBoard.indexOfLastStone(8), 0);
	}
	
	@Test
	public void testMakeSingeMove() {
		KalahaGame updatedTestBoard = testBoard.makeSingleMove(8);
		assertEquals(updatedTestBoard.isHuman(), true);
		assertEquals(updatedTestBoard.getPit(0), 7);
	}
}
