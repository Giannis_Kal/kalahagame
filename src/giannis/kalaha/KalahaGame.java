package giannis.kalaha;

public class KalahaGame {
	
	private int[] pitList;
	private boolean human;
	
	private static final int defaultNUmberOfPits = 6;
	private static final int defaultStonesPerPit = 6;
	private static final boolean defaultFirstPlayerHuman = false;
	
    /**
     * Instantiates a new Board based on a certain number of stones,
     * number of pits and who can make a move first
     * @param stones the stones inside each pit
     * @param pits   the number of playing pits for each player
     * @param h      is true if Human can make the first move.
     */
	public KalahaGame(int stones, int pits, boolean h){
		this.pitList = new int[2*pits + 2];
		this.human = h;
		InitializePits(pits, stones);
	}
	
    /**
     * Initiates a new Board according to default values
     */
	public KalahaGame(){
		this.pitList = new int[2*defaultNUmberOfPits + 2];;
		this.human = defaultFirstPlayerHuman;
		InitializePits(defaultNUmberOfPits, defaultStonesPerPit);
	}
	
    /**
     * Initiates a new Board that creates a deep copy of another Board
     * This constructor updates the Board after a single move
     * @param newBoard The game to be copied
     */
    public KalahaGame(KalahaGame updatedBoard) {
        this.pitList = new int[updatedBoard.pitList.length];
        for (int i = 0; i < updatedBoard.pitList.length; ++i) {
            setPit(i, updatedBoard.getPit(i));
        }
        this.human = updatedBoard.isHuman();
    }
    
    public void InitializePits(int pits, int stones) {
		for(int j=0; j<pits; j++) {
			setPit(j, (int) stones);
			setPit(pits+1+j, (int) stones);
		}
		setPit(pits, 0);
		setPit(2*pits+1, 0);
    }
    
    public void setPit(int index, int value) {
        this.pitList[index] = value;
    }
    
    public int getPit(int i) {
        return pitList[i];
    }
    
    public int humanPit() {
        return pitList.length/2-1;
    }

    public int robotPit() {
        return pitList.length-1;
    }
    
    public boolean isHuman() {
        return human;
    }
    
    /**
     * Gets current score of the game
     *
     * @return the number of stones ahead from Human view point
     */
    public int getScore() {
        return (getPit(humanPit()) - getPit(robotPit()));
    }

    public int nextPit(int i, boolean h) {
        i = (i == robotPit()) ? 0 : i+1;
        if (h && i == robotPit()) i = 0;
        if (!h && i == humanPit()) i = humanPit()+1;
        return i;
    }
    
    public boolean gameOver() {
        int hum = 0, rob = 0;
        for (int i = 0; i < humanPit(); i++) {
            hum += getPit(i);
            rob += getPit(i+humanPit()+1);
        }
        return (hum == 0 || rob == 0);
    }
    
    public void AllocateLastStones() {
        for (int pos = 0; pos < humanPit(); ++pos) {
            setPit(humanPit(), getPit(humanPit())+getPit(pos));
            setPit(pos, 0);
            setPit(robotPit(), getPit(robotPit())+getPit(pos+humanPit()+1));
            setPit(pos+humanPit()+1, 0);
        }
    }
    
    public boolean isWinPit(int i) {
        return i == humanPit() || i == robotPit();
    }
    
    public boolean isHumanLargePit(int i) {
        return i == humanPit();
    }
    
    public boolean isRobotLargePit(int i) {
        return i == robotPit();
    }

    public void switchPlayer() {
    	this.human = !isHuman();
    }
    
    public boolean GameIsDrawn() {
        return (getScore() == 0);
    }
    
    public KalahaGame makeSingleMove(int i) {
        int stones = getPit(i);
        this.setPit(i, 0);
        while (stones != 0) {
            i = this.nextPit(i, this.isHuman());
            this.setPit(i, this.getPit(i)+1);
            --stones;
        }
        this.captureOperation(i);
        if (this.gameOver())
        	this.AllocateLastStones();

        if (!this.isWinPit(i))
        	this.switchPlayer();
        return this;
    }
    
    public void captureOperation(int i) {
        if (!isWinPit(i) && getPit(i) == 1 && (isHuman() && i < humanPit() || !isHuman() && i > humanPit())) {
            setPit(humanPit()+getOffset(), getPit(humanPit()+getOffset()) + getPit(i) + getPit(oppositePit(i)));
            setPit(i, 0);
            setPit(oppositePit(i), 0);
        }
    }
        /**
         * Gets the Offset between Humans and Robots Pit indices
         *
         * @return the Offset between the indices
         */
    public int getOffset() {
           return (this.isHuman()) ? 0 : pitList.length/2;
    }
    
    public int indexOfLastStone(int pit) {
        for (int stones = this.getPit(pit); stones > 0; --stones) {
            pit = this.nextPit(pit, this.isHuman());
        }
        return pit;
    }
    
    public int oppositePit(int i) {
        return 2*humanPit()-i;
    }
	
    /**
     * Prints the current Board to the console. For Test purposes only
     */
    public void printBoard() {
        System.out.printf("Current board status:\n\n");
        for (int i = robotPit()-1; i > humanPit(); --i) {
            System.out.printf("\t"+getPit(i));
        }
        int humanPit = getPit(humanPit());
        int robotPit = getPit(robotPit());
		System.out.print("\n\n" + robotPit);		
		System.out.println("\t\t\t\t\t\t\t" +humanPit + "\n");
        for (int i = 0; i <= humanPit()-1; ++i) {
            System.out.printf("\t"+getPit(i));
        }
        System.out.print("\n\n");
        System.out.printf("==================ROUND FINISHED=================");
        System.out.print("\n\n");
    }
}
