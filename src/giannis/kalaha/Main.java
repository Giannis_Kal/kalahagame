package giannis.kalaha;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.io.IOException;
import java.util.InputMismatchException;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		KalahaGame board=new KalahaGame();
		printInstructions();
		board.printBoard();
		do {
			computerMove(board);
			humanMove(board);
		}while (!board.gameOver());
		printResult(board);
	}
	
	public static void printInstructions()
	{	int index;
	System.out.println("\n\t\t\t\t\tWelcome to Kalaha Game!\n");
	System.out.println("\nObjective of the game:\n\n\tEach player attempts to collect as many stones as possible.");
	System.out.println("\nThe Board:\n\n\tEach side, top and bottom, belongs to a different player.\n\tThe six pits nearest to each player belong to him or her\n\tand each player's Large Pit is to the right.");
	System.out.println("\nHow to play:\n\n\tPlayers alternate turns.\n\tIn his or her turn each player selects a pit of stones from one\n\tbox on his or her side of the board.\n\tEach stone is placed one by one in the pits around the board,\n\tincluding his Large Pit but not the opponent's Large Pit.\n\tIf the last stone lands into the player's own Kalaha,\n\tthat player gets an extra turn.\n\tIf the last stone lands in an empty pit on the player's own side,\n\the captures all the\n\tstones from the opponent's pit directly opposite that pit.");
	System.out.println("\nHow to win:\n\n\tThe game is over when a player has no more pits in play on the board.\n\tThe winner is the player with the greatest total of stones in his Large Pit\n\tand any remaining stones on the opponents side of the board.");
		System.out.println("\n\tThese are the indices for the various pits. We will be using these in the game\n");
		for(index=12;index>6;index--)
		{
			System.out.print("\t"+index);		
		}
		System.out.print("\n\n 13");		
		System.out.println("\t\t\t\t\t\t\t6\n");
		for(index=0;index<6;index++)
		{
			System.out.print("\t"+index);		
		}
		System.out.println("\n");
		System.out.println("Pit 13 is Computer's Large Pit and Pit 6 is Human's Large Pit. You will be choosing between number 0 and 5, one per round.");
	System.out.println("===================GAME STARTS!====================");
	}
	
	public static void printResult(KalahaGame board){
		if(board.getScore()>0) {
			System.out.print("Congratulations, you have won!");
		}else if(board.getScore()==0) {
			System.out.print("Tough luck, you lost!");
		} else {
			System.out.print("The game has been tied!");
		}
	}
	
	public static void computerMove(KalahaGame board) throws InterruptedException {
		System.out.println("COMPUTER'S TURN, HE IS THINKING...");
		Thread.sleep(2000);
		int computerChoice = ThreadLocalRandom.current().nextInt(board.humanPit()+1, board.robotPit());
		int indexOfLastStone = board.indexOfLastStone(computerChoice);
		System.out.println("Computer chooses tip " + computerChoice  + " , updating board...");
		board.makeSingleMove(computerChoice).printBoard();
		
		while (board.isWinPit(indexOfLastStone)) {
			System.out.println("COMPUTER REPLAYS, THINKING...");
			Thread.sleep(2000);
			int nextChoice = ThreadLocalRandom.current().nextInt(board.humanPit()+1, board.robotPit());
			indexOfLastStone = board.indexOfLastStone(nextChoice);
			System.out.println("His next choice is tip " + nextChoice + " , updating board...");
			board.makeSingleMove(nextChoice).printBoard();
		}
	}
	
	public static void humanMove(KalahaGame board) {
		try { 
			System.out.println("YOUR TURN! Please choose a non-empty pit: ");
			Scanner humanInput = new Scanner(System.in);
			int HumanChoice = humanInput.nextInt();

			System.out.println("You have chosen tip " + HumanChoice + " , updating board...");
			int indexOfLastStone = board.indexOfLastStone(HumanChoice);
			board.makeSingleMove(HumanChoice).printBoard();
		
			while (board.isWinPit(indexOfLastStone)) {
				System.out.println("YOU PLAY AGAIN! Please choose a non-empty tip:");
				int nextChoice = humanInput.nextInt();
				System.out.println("Your have chosen tip " + nextChoice + " , updating board...");
				indexOfLastStone = board.indexOfLastStone(nextChoice);
				board.makeSingleMove(nextChoice).printBoard();
			}
		}catch(InputMismatchException ex) {
			System.out.println("You need to select integer number between 0 and 5.");
			System.out.println("Everything else interrupts the game. Please try again.");
			System.exit(0);
		}
	}
}
